export default class Socket {
	
	nickname;
	isConnected = false;
	
	constructor(socket) {
		this.callbackIsConnected = () => {};
		this.socket = socket;
		
		this.socket.on('connect', () => {
			this.connected = true;
			
			const nickname = localStorage.getItem('nickname');
			
			if(!!nickname) {
				this.setNickname$(nickname)
					.then(() => {
						this.callbackIsConnected(!!nickname);
					});
			} else {
				this.callbackIsConnected(false);
			}
		});
	}
	
	getNickname() {
		return this.nickname;
	}
	
	setNickname$(nickname) {
		return new Promise((resolve, reject) => {
			this.socket.emit('client/nickname', nickname);
			this.socket.on('client/nickname/success', (nickname) => {
				localStorage.setItem('nickname', nickname);
				this.nickname = nickname;
				resolve(nickname);
			});
			
		});
		
	}
	subscribeIsConnected$(callback) {
		if(this.connected) {
			callback();
		} else {
			this.callbackIsConnected = callback;
		}
	}
	
	subscribeRooms$(callback) {
		this.socket.emit('rooms');
		this.socket.on('rooms', (rooms) => {
			callback(rooms);
		});
	}
	
	setTeam$(roomId, team) {
		return new Promise((resolve, reject) => {
			this.socket.emit(`room/${roomId}/player/setTeam`, team);
			
			this.socket.on(`room/${roomId}/player/setTeam/success`, () => {
				resolve(team);
			});
		});
	}
	
	createRoom$(room) {
		return new Promise((resolve, reject) => {
			this.socket.emit('room/create', room);
			this.socket.on('room/create/success', (roomData) => {
				resolve(roomData);
			});
			this.socket.on('room/create/error', (error) => {
				reject(error);
			});
		});
	}
	
	connectRoom$(roomId) {
		return new Promise((resolve, reject) => {
			console.log('connecting to ' , roomId);
			this.socket.emit('room/connect', roomId);
			this.socket.on('room/connect/success', () => {
				resolve();
			});
			this.socket.on('room/connect/error', (roomId) => {
				reject();
			});
		});
	}
	
	subscribeRoomPlayers$(roomId, callback) {
		console.log('subscribeRoomPlayers', roomId);
		this.socket.on(`room/${roomId}/players`, (players) => {
			console.log('Players updated', players);
			callback(players);
		});
	}
	
	roomStart$(roomId) {
		return new Promise((resolve, reject) => {
			this.socket.emit(`room/${roomId}/start`, {duration: 180});
			console.log('/START');
			this.socket.on(`room/${roomId}/start/success`, () => {
				resolve();
			});
			this.socket.on(`room/${roomId}/start/error`, (error) => {
				reject(error);
			});
		});
	}
	
	subscribeRoomData$(roomId, callback) {
		this.socket.on(`room/${roomId}/data`, callback);
	}
}