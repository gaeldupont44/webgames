import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import "./App.css";
import "./GUI.css";

import {BACK} from "./config.js";

import Socket from "./socket.js";
import Home from "./pages/home/Home.js";
import Rooms from "./pages/rooms/Rooms.js";
import SoccerRoom from "./pages/soccer-room/SoccerRoom.js";
import LoveLetterRoom from "./pages/love-letter-room/LoveLetterRoom.js";

import socketIOClient from "socket.io-client";

import {Redirect} from "react-router-dom";

class App extends React.Component {
	
	constructor() {
		super();
		this.state = {
    		socket: new Socket(socketIOClient(BACK.WEBSOCKET)),
    		isConnected: false,
    		hasNickname: false
    	};
	}
	
	componentWillMount() {
    	console.log('SOCKET', this.state.socket);
    	this.state.socket.subscribeIsConnected$((hasNickname) => {
    		this.setState({isConnected: true, hasNickname});
    		console.log('socket connected', this.state);
    	});
    	
	}
  
  render () {
  	let redirect = (isForced) => {
  		if(this.state.isConnected) {
  			if (!this.state.hasNickname) {
  				return (<Redirect to='/home' />);
    		} else if(isForced) {
    			return (<Redirect to='/rooms' />);
    		}
  		}
  		return null;
  	};
  	return (
  		<div className="App">
		  	<Router>
		  		{redirect()}
		    	<Switch>
		       		<Route path="/home">
		           		<Home socket={this.state.socket} />
		       		</Route>
		       		<Route exact path="/rooms">
		           		<Rooms socket={this.state.socket} />
		       		</Route>
		       		<Route path="/room-soccer/:id" render={(props) => <SoccerRoom {...props} socket={this.state.socket} />}></Route>
		       		
		       		<Route path="/room-love_letter/:id" render={(props) => <LoveLetterRoom {...props} socket={this.state.socket} />}></Route>
		       		
		       		<Route exact path="*">
		           		{redirect(true)}
		       		</Route>
		    	</Switch>
		    </Router>
	    </div>
    );
  };
}

export default App;
