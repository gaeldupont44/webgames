import React from 'react';

import "./AppHeader.css";

import {Redirect} from "react-router-dom";

class AppHeader extends React.Component {
  
  constructor (props) {
  	super(props);
  	
  	this.state = {
  		mustRedirectBack: false
  	};

  }
  
  onRedirectBack() {
  	this.setState({mustRedirectBack: true});
  }
  
  render () {
  	if (this.state.mustRedirectBack) {
   		return (<Redirect to={`/rooms`} />);
    }
  	return (
		<div className="AppHeader">
			<div className="AppHeader__redirect-back">
			{/* Bouton de retour au salle */}
				{!this.props.hideBackButton ?
					<span onClick={() => this.onRedirectBack()}>&larr;</span> :
					null
				}
			</div>
    		<div className="AppHeader__title">{this.props.title}</div>
    		<div className="AppHeader__info">{this.props.children}</div>
   		</div>
    );
  };
}

export default AppHeader;
