import React from 'react';

import "./Modal.css";

class Modal extends React.Component {
  
  constructor (props) {
  	super(props);
  	console.log('props', props)
  	
  	this.state = {
  	};

  }
  
  
  render () {
  	return (
		<div className="Modal">
    		<div className="Modal-close" onClick={this.props.onClose}></div>
    		<div className="Modal-content">{this.props.children}</div>
   		</div>
    );
  };
}

export default Modal;
