import React from 'react';

import {Redirect} from "react-router-dom";

import './Rooms.css';

import Modal from "../../components/Modal/Modal.js";

import AppHeader from "../../components/AppHeader/AppHeader.js";

import CreateRoomModal from "./create-room-modal/CreateRoomModal.js";

class Rooms extends React.Component {
  
  constructor (props) {
  	super(props);
  	
  	this.state = {
  		data: [],
  		mustRedirectHome: false,
  		showModal: false,
  		toRoomType: null,
  		toRoomId: null
  	};

	this.openCreateRoomModal = this.openCreateRoomModal.bind(this);
	this.closeCreateRoomModal = this.closeCreateRoomModal.bind(this);
	this.goToRoom = this.goToRoom.bind(this);
	this.subscribeRoomsHandler = this.subscribeRoomsHandler.bind(this);
  }
  
  componentWillReceiveProps(newProps) {
  	this.props.socket.socket.off('rooms', this.subscribeRoomsHandler);
  	newProps.socket.socket.on('rooms', this.subscribeRoomsHandler);
  	newProps.socket.socket.emit('rooms');
  }
  
  componentDidMount() {
  	this.props.socket.socket.on('rooms', this.subscribeRoomsHandler);
  	this.props.socket.socket.emit('rooms');
  }
  
  componentWillUnmount() {
  	this.props.socket.socket.off('rooms', this.subscribeRoomsHandler);
  }
  
  openCreateRoomModal() {
  	this.setState({
  		showModal: true
  	});
  }
  
  closeCreateRoomModal() {
  	this.setState({
  		showModal: false
  	});
  }

  goToHome() {
  	this.setState({mustRedirectHome: true});
  }
  
  goToRoom(roomType, roomId) {
  	if(this.state.showModal) {
  		this.closeCreateRoomModal();
  	}
  	this.setState({
	  	toRoomType: roomType,
		toRoomId: roomId
	});
  }
  
  subscribeRoomsHandler(rooms) {
	this.setState({data: rooms});
  }
  
  render () {
    
	if (!!this.state.toRoomType && !!this.state.toRoomId) {
   		return (<Redirect to={`/room-${this.state.toRoomType.toLowerCase()}/${this.state.toRoomId}`} />);
    }
    if(this.state.mustRedirectHome) {
    	return (<Redirect to="/home" />);
    }
    
    let modal;
    if(this.state.showModal) {
    	modal = <Modal className="Rooms-create-room-modal" onClose={this.closeCreateRoomModal}>
    				<CreateRoomModal socket={this.props.socket} onSubmit={this.goToRoom}></CreateRoomModal>
    			</Modal>;
    }
    
    let data = this.state.data.map((room) => <div key={room.id} onClick={() => this.goToRoom(room.type, room.id)}>{room.type} - {room.name} ({room.nbPlayer} joueurs connectés)</div>);
    
  	return (
  		
	    <section className="Rooms">
	    	<AppHeader title="WebGames" hideBackButton={true}>
	    		<a href="#" className="Rooms__redirect-home" onClick={() => this.goToHome()}>Changer de pseudo</a>
	    	</AppHeader>
	    	
			<h1>Liste des salles</h1>
			{data.length > 0 ? <div className="Rooms__room">{data}</div> : <span>Aucune salle d'ouverte pour le moment</span>}
			<button className="button" onClick={this.openCreateRoomModal}>Créer une salle</button>
			{modal}
	    </section>
    );
  };
}

export default Rooms;
