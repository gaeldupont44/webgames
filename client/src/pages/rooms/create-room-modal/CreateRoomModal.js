import React from 'react';

import './CreateRoomModal.css';

class CreateRoomModal extends React.Component {
  
  constructor (props) {
  	super(props);
  	
  	this.state = {
  		name: '',
  		password: '',
  		type: ''
  	};

	this.handleCreateRoomModalNameChange = this.handleCreateRoomModalNameChange.bind(this);
	this.handleCreateRoomModalPasswordChange = this.handleCreateRoomModalPasswordChange.bind(this);
	this.handleCreateRoomModalTypeChange = this.handleCreateRoomModalTypeChange.bind(this);
	this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  async handleSubmit(event) {
  	event.preventDefault();
  	if(!this.checkNameValidity(this.state.name) && !this.checkPasswordValidity(this.state.password) && !this.checkTypeValidity(this.state.type)) {
  		const {type, id} = await this.props.socket.createRoom$(this.state);
  		this.props.onSubmit(type, id);
  	}
  }
  
  checkNameValidity(name) {
  	if(name.length === 0) {
  		return "required";
  	}
  	if(name.length < 5) {
  		return "min-char";
  	}
  	return;
  }
  
  checkPasswordValidity(password) {
  	if(password.length === 0) {
  		return "required";
  	}
  	if(password.length < 5) {
  		return "max-char";
  	}
  	return;
  }
  
  checkTypeValidity(type) {
  	if(!type) {
  		return "required";
  	}
  	return;
  }
  
  handleCreateRoomModalNameChange(event) {
  	this.setState({
  		name: event.target.value
  	});
  }
  
  handleCreateRoomModalPasswordChange(event) {
  	this.setState({
  		password: event.target.value
  	});
  }
  
  handleCreateRoomModalTypeChange(event) {
  	this.setState({
  		type: event.target.value
  	});
  }
  
  render () {
  	return (
	    <form className="CreateRoomModal" onSubmit={this.handleSubmit}>
	    	<h1>Nouvelle Salle</h1>
	    	<div className="CreateRoomModal-fields">
	    		<div className="CreateRoomModal-name">
	    			<label>Nom</label>
	    			<input className="input" type="text" placeholder="Nom" value={this.state.name} onChange={this.handleCreateRoomModalNameChange} />
	    		</div>
	    		
	    		<div className="CreateRoomModal-password">
	    			<label>Mot de passe</label>
	    			<input className="input" type="password" placeholder="Mot de passe" value={this.state.password} onChange={this.handleCreateRoomModalPasswordChange} />
	    		</div>
	    		<div className="CreateRoomModal-type">
	    			<label>Jeu</label>
	    			<select defaultValue="" onChange={this.handleCreateRoomModalTypeChange}>
	    				<option value="">Séléctionner</option>
	    				<option value="SOCCER">Soccer</option>
	    				<option value="LOVE_LETTER">Love Letter</option>
	    			</select>
	    		</div>
    		</div>
    		<div>
    			<button type="submit" className="button">Créer</button>
    		</div>
    	</form>
    );
  };
}

export default CreateRoomModal;
