import React from 'react';

import {Redirect} from "react-router-dom";

import './SoccerRoom.css';

class SoccerRoom extends React.Component {
  
  constructor (props) {
  	super(props);
  	
  	this.state = {
  		players: [],
  		teams: [
  			{
  				id: "RED",
  				value: "Rouge"
  			},
	  		{
  				id: "BLUE",
  				value: "Bleue"
  			}
  		],
  		data: null
  	};
  	
  	this.setTeam = this.setTeam.bind(this);
  	this.start = this.start.bind(this);

  }

  componentDidMount() {

  	this.getPlayers();
  	this.props.socket.subscribeRoomData$(this.props.match.params.id, (roomData) => {
  		console.log('ROOM DATA', roomData);
 		this.setState({
 			data: roomData
 		});
  	});
  }
  
  getPlayers() {
  	this.props.socket.subscribeRoomPlayers$(this.props.match.params.id, (players) => {
  		this.setState({
  			players: players
  		});
  	});
  }
  
  setTeam(team) {
  	this.props.socket.setTeam$(this.props.match.params.id, team.id);
  }
  
  start() {
  	this.props.socket.roomStart$(this.props.match.params.id);
  }
  
  render () {
  	if (this.state.toHome) {
   		return (<Redirect to='/' />);
    }
    
    let templateWaiting = (
    	<div className="Room-waiting-players">
	    	<div className="Room-teams">
	    		 {this.state.teams.map((team) => <div key={team.id}  className={`Room-teams-team Room-teams-team--${team.id.toLowerCase()}`} onClick={()=> this.setTeam(team)}>{team.value}</div>)}
	    	</div>
	    	<div className="Room-players">
	    		<div className="Room-players-team Room-players-team--red">
	    			<div className="Room-players-teams">ROUGES</div>
	    			
	    			{this.state.players.filter((player) => player.team === "RED").map((player) => <div key={player.id} className="Room-players-player">{player.nickname}</div>)}
	    		</div>
	    		<div className="Room-players-team Room-players-team--spectator">
	    			<div className="Room-players-teams">SPECATEURS</div>
	    			{this.state.players.filter((player) => !player.team).map((player) => <div key={player.id} className="Room-players-player">{player.nickname}</div>)}
	    		</div>
	    		<div className="Room-players-team Room-players-team--blue">
	    			<div className="Room-players-teams">BLEUS</div>
	    			{this.state.players.filter((player) => player.team === "BLUE").map((player) => <div key={player.id} className="Room-players-player">{player.nickname}</div>)}
	    		</div>
	        </div>
	        <div className="Room-start">
	        	<button className="button" onClick={() => this.start()}>Lancer la partie</button>
	        </div>
    	</div>
    )
    
    let templateStarted = (
    	<div className="Room-playing">
			HELLO WORLD
		</div>
	)
    let template;
    switch(this.state.data && this.state.data.state) {
    	case "WAITING_PLAYERS": template = templateWaiting; break;
    	case "PLAYING": template = templateStarted; break;
    	default: template = templateWaiting;
    }
  	return (
    <section className="Room">
    	{template}
    </section>
    );
  };
}

export default SoccerRoom;
