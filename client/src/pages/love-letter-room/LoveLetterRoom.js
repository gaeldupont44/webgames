import React from 'react';

import { Redirect } from "react-router-dom";
import Modal from "../../components/Modal/Modal.js";
import AppHeader from "../../components/AppHeader/AppHeader.js";
import Board from './composants/Board/Board.js';
import Infos from './composants/Infos/Infos.js';

import './LoveLetterRoom.css';

class LoveLetterRoom extends React.Component {

	constructor(props) {		
		super(props);

		this.state = {
			roomId: this.props.match.params.id,
			toRooms: false,
			player: null,
			players: [],
			roomState: {name: "WAITING_PLAYERS"},
			isApplyingEffect: false,
			actionOneTargetPlayerId: null,
			actionSixCardsIndex: [],
			showTargetPlayerData: null,
			showActionOneModal: null,
			socket: this.props.socket.socket,
			history: []
		};
		
		window.onbeforeunload = (event) => {
			this.props.socket.socket.emit('room/disconnect', this.state.roomId);
			this.props.socket.socket.off('rooms', this.subscribeRoomsHandler);
    	};
    	
		this.start = this.start.bind(this);
		this.takeCard = this.takeCard.bind(this);
		this.applyCardEffect = this.applyCardEffect.bind(this);
		this.setActionOneTargetPlayerId = this.setActionOneTargetPlayerId.bind(this);
		this.removeTargetPlayerData = this.removeTargetPlayerData.bind(this);
		this.onPlayerSelection = this.onPlayerSelection.bind(this);
		
		this.subscribePlayersHandler = this.subscribePlayersHandler.bind(this);
		this.subscribeRoomStateHandler = this.subscribeRoomStateHandler.bind(this);
		this.subscribeHistoryHandler = this.subscribeHistoryHandler.bind(this);
		this.subscribePlayerHandler = this.subscribePlayerHandler.bind(this);
		this.subscribeDeckHandler = this.subscribeDeckHandler.bind(this);
		this.subscribeCurrentCards = this.subscribeCurrentCards.bind(this);
		this.subscribeShowPlayerCurrentCard = this.subscribeShowPlayerCurrentCard.bind(this);
	}

	componentWillReceiveProps(newProps) {
		this.unsubscribeAll(this.props, this.state.roomId);
		this.subscribeAll(newProps, this.state.roomId);
	}
	
	componentDidMount() {
		this.subscribeAll(this.props, this.state.roomId);
		this.props.socket.socket.emit('room/connect', this.state.roomId);
	}
	
	componentWillUnmount() {
		this.unsubscribeAll(this.props, this.state.roomId);
		this.props.socket.socket.emit('room/disconnect', this.state.roomId);
	}
	
	subscribeAll(props, roomId) {
		props.socket.socket.on(`room/${roomId}/players`, this.subscribePlayersHandler);
		props.socket.socket.on(`room/${roomId}/state`, this.subscribeRoomStateHandler);
		props.socket.socket.on(`room/${roomId}/history`, this.subscribeHistoryHandler);
		props.socket.socket.on(`room/${roomId}/player`, this.subscribePlayerHandler);
		props.socket.socket.on(`room/${roomId}/deck`, this.subscribeDeckHandler);
		props.socket.socket.on(`room/${roomId}/player/currentCards`, this.subscribeCurrentCards);
		props.socket.socket.on(`room/${roomId}/player/showPlayerCurrentCard`, this.subscribeShowPlayerCurrentCard);
	}
	
	unsubscribeAll(props, roomId) {
		props.socket.socket.off(`room/${roomId}/players`, this.subscribePlayersHandler);
		props.socket.socket.off(`room/${roomId}/state`, this.subscribeRoomStateHandler);
		props.socket.socket.off(`room/${roomId}/history`, this.subscribeHistoryHandler);
		props.socket.socket.off(`room/${roomId}/player`, this.subscribePlayerHandler);
		props.socket.socket.off(`room/${roomId}/deck`, this.subscribeDeckHandler);
		props.socket.socket.off(`room/${roomId}/player/currentCards`, this.subscribeCurrentCards);
		props.socket.socket.off(`room/${roomId}/player/showPlayerCurrentCard`, this.subscribeShowPlayerCurrentCard);
	}

	subscribePlayersHandler(players) {
		this.setState({players});
	}
	
	subscribeRoomStateHandler(roomState) {
		const isApplyingEffect = roomState.name === "APPLYING_CARD_EFFECT" && roomState.activePlayerId === this.state.player?.id;
		this.setState({ roomState, isApplyingEffect, actionOneTargetPlayerId: null, actionSixCardsIndex: [] });
	}
	
	subscribeHistoryHandler(history) {
		this.setState({ history });
	}
	
	subscribePlayerHandler(player) {
		this.setState({player});
	}
	
	subscribeDeckHandler(deck) {
		this.setState({deck});
	}
	
	subscribeCurrentCards(currentCards) {
		this.setState({currentCards});
	}
	
	subscribeShowPlayerCurrentCard(showTargetPlayerData) {
		this.setState({ showTargetPlayerData });
		setTimeout(() => {
			this.removeTargetPlayerData();
		}, 10000);
	}
	
	removeTargetPlayerData() {
		this.setState({ showTargetPlayerData: null });
	}

	start() {
		this.props.socket.roomStart$(this.state.roomId);
	}

	useCurrentCard(currentCardIndex) {
		this.props.socket.socket.emit(`room/${this.state.roomId}/player/useCurrentCard`, currentCardIndex);
	}

	applyCardEffect(data) {
		this.props.socket.socket.emit(`room/${this.state.roomId}/player/applyCardEffect`, data);
	}

	takeCard() {
		this.props.socket.socket.emit(`room/${this.state.roomId}/player/takeCard`);
	}

	setActionOneTargetPlayerId(playerId) {
		this.setState({ actionOneTargetPlayerId: playerId });
	}
	
	onPlayerSelection(playerId) {
		(this.state.player.lastUsedCard === 1) ? this.setActionOneTargetPlayerId(playerId) : this.applyCardEffect({playerId});
	}
	
	getPlayer(playerId) {
		return this.state.players.find((player) => player.id === playerId);
	}

	render() {
		if (this.state.toRooms) {
			return (<Redirect to='/rooms' />);
		}

		let showActionOneModal = () => {
			if (this.state.isApplyingEffect && (this.state.player.lastUsedCard === 1) && !!this.state.actionOneTargetPlayerId) {

				const cards = [];

				for (let card = 0; card <= 9; card++) {
					if (card !== 1) {
						cards.push(<img key={card} className="Room-love-letter-action-one-modal__cards__card" src={`/love-letter/${card}.png`} alt={card} onClick={() => this.applyCardEffect({ playerId: this.state.actionOneTargetPlayerId, card: card })} />);
					}
				}

				return <Modal className="Room-love-letter-action-one-modal" onClose={() => this.setActionOneTargetPlayerId(null)}>
					<div className="Room-love-letter-action-one-modal__cards">
						{cards}
					</div>
				</Modal>;
			}
		};

		let showTargetPlayerModal = () => {
			if (!!this.state.showTargetPlayerData) {
				return <Modal className="Room-love-letter-modal" onClose={() => this.removeTargetPlayerData()}>
					<div className="Room-love-letter-modal__card" style={{ fontSize: "50px", backgroundImage: `url(/love-letter/${this.state.showTargetPlayerData?.targetPlayerCurrentCard}.png)` }}>{this.state.showTargetPlayerData?.targetPlayerCurrentCard}</div>
				</Modal>;
			}
		};

		const currentPlayer = this.state.players.find((player) => player.id === this.state.player?.id);
		const showStartGameButton = currentPlayer?.isAdmin && (this.state.roomState?.name === "WAITING_PLAYERS" || this.state.roomState?.name === "END_GAME");

		return (
			<section className="Room-love-letter">
				<AppHeader title="Love Letter"></AppHeader>
				<div className="Room-love-letter__content">
					<Board
						id={this.state.roomId}
						players={this.state.players}
						player={this.state.player}
						deck={this.state.deck}
						socket={this.props.socket}
						isApplyingEffect={this.state.isApplyingEffect}
						roomState={this.state.roomState}
						currentPlayerData={this.state.currentPlayerData}
						onDeckCardClicked={() => {this.takeCard();}}
						onPlayerSelection={(playerId) => {this.onPlayerSelection(playerId);} }
					></Board>
	
					<Infos 
						showStartGameButton={showStartGameButton}
						players={this.state.players}
						player={this.state.player}
						history={this.state.history} 
						isApplyingEffect={this.state.isApplyingEffect}
						roomState={this.state.roomState}
						onStartButtonClicked={() => {this.start();}}
						onActionSixCardsSelected={(actionSixCardsIndex) => {this.applyCardEffect(actionSixCardsIndex);}}
						onCurrentCardClicked={(index) => {this.useCurrentCard(index);}}
					></Infos>
				</div>

				{showActionOneModal()}
				{showTargetPlayerModal()}
			</section>
		);
	};
}

export default LoveLetterRoom;