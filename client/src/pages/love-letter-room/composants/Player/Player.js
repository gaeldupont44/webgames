import React from 'react';

import "./Player.css";

class Player extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			player: props.player,
			isApplyingEffect: props.isApplyingEffect,
			isPlaying: props.isPlaying,
			onSelection: props.onSelection
		};		
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			player: nextProps.player,
			isApplyingEffect: nextProps.isApplyingEffect,
			isPlaying: nextProps.isPlaying,
			onPlayerSelected: nextProps.onPlayerSelected,
			onSelection: nextProps.onSelection
		});
	}

	getPlayerClassList() {
		const playerStatusClass = this.state.player.isAlive ? 'Player--alive' : 'Player--dead';
		const playerActiveClass = this.state.isPlaying ? 'Player--active' : '';
		const playerSelectableClass = this.isSelectable() ? 'Player--selectable' : '';
		return `Player ${playerStatusClass} ${playerActiveClass} ${playerSelectableClass}`;
	}

	isSelectable() {
		const isPlayerProtected = this.state.player.lastUsedCard === 4 && !this.state.player.isLastWithoutEffect;
		const isLastCardSix = this.state.isPlaying && this.state.player?.lastUsedCard === 6;
		const isLastCardFive = this.state.isPlaying && this.state.player?.lastUsedCard === 5;
		const isSelectable = (this.state.isApplyingEffect && !isLastCardSix && !isPlayerProtected && (!this.state.isPlaying || isLastCardFive));
		return isSelectable;
	};

	render() {
		return (
			<div className={this.getPlayerClassList()} id={this.state.player.id} onClick={() => this.isSelectable() && this.props.onSelection(this.state.player.id)}>
				<div className="Player-name">{this.state.player.nickname}</div>
				<div className="Player-cards">{Array.apply(null, Array(this.state.player.nbCurrentCards)).map((card, index) => <div className="Player-card" key={`Player-${this.state.player.id}-card-${index}`}></div>)}</div>
				<div className="Player-used-cards">
					{this.state.player.usedCards.map((card, index) => <div className="Player-used-card" key={`Player-${this.state.player.id}-used-card-${index}`}>{card}</div>)}
				</div>
				<div className="Player-tokens">
					{Array.apply(null, Array(this.state.player.win)).map((token, index) => <div className="Player-token" key={`Player-${this.state.player.id}-token-${index}`}></div>)}
				</div>
			</div>
		);
	};
}

export default Player;
