import React from 'react';

import "./Infos.css";

class Infos extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			showStartGameButton: props.showStartGameButton,
			player: props.player,
			players: props.players,
			history: props.history,
			isApplyingEffect: props.isApplyingEffect,
			roomState: props.roomState,
			onStartButtonClicked: props.onStartButtonClicked,
			onActionSixCardsSelected: props.onActionSixCardsSelected,
			onCurrentCardClicked: props.onCurrentCardClicked,
			actionSixCardsIndex: []
		};

		this.setActionSixCardsIndex = this.setActionSixCardsIndex.bind(this);
		this.onActionSixButtonClicked = this.onActionSixButtonClicked.bind(this);
		this.getActionMessage = this.getActionMessage.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			showStartGameButton: nextProps.showStartGameButton,
			player: nextProps.player,
			players: nextProps.players,
			history: nextProps.history,
			isApplyingEffect: nextProps.isApplyingEffect,
			roomState: nextProps.roomState,
			onStartButtonClicked: nextProps.onStartButtonClicked,
			onActionSixCardsSelected: nextProps.onActionSixCardsSelected,
			onCurrentCardClicked: nextProps.onCurrentCardClicked,
		});
		
		this.scrollHistoryToBottom();
	}

	setActionSixCardsIndex(cardIndex) {
		if (this.state.player.lastUsedCard === 6) {
			// To DO : Gérer le cas ou il ne reste plus qu'une carte dans le deck et du coup 2 cartes au lieu de trois | DONE ?
			if (!this.state.actionSixCardsIndex.includes(cardIndex) && (this.state.actionSixCardsIndex.length < (this.state.player?.currentCards.length - 1))) {
				this.setState({
					actionSixCardsIndex: [...this.state.actionSixCardsIndex, cardIndex]
				});
			} else {
				this.setState({
					actionSixCardsIndex: this.state.actionSixCardsIndex.filter((_cardIndex) => _cardIndex !== cardIndex)
				});
			}
		}
	}
	
	onActionSixButtonClicked() {
		this.state.onActionSixCardsSelected(this.state.actionSixCardsIndex);
		this.setState({actionSixCardsIndex: []});
	}
	
	
	getActionMessage(historyItem, index) {
		
		const getCardName = (card) => {
			switch (card) {
				case 0: return "Espionne";
				case 1: return "Garde";
				case 2: return "Prêtre";
				case 3: return "Baron";
				case 4: return "Servante";
				case 5: return "Prince";
				case 6: return "Chancelier";
				case 7: return "Roi";
				case 8: return "Comtesse";
				case 9: return "Princesse";
				default: return "";
			}
		};
		
		const getPlayer = (playerId) => {
			return this.state.players.find((player) => player.id === playerId);
		};
		
		const getClass = () => {
			let classes = ["Infos__history__message"];
			switch(historyItem.name) {
				case "KILL_PLAYER":
					classes.push(classes[0] + '--kill');
					break;
				case "END_GAME":
					classes.push(classes[0] + '--win');
			}
			return classes.join(' ');
		};
		
		const getMessage = () => {
			if (historyItem && this.state.players && this.state.players.length > 0) {
				const adminNickname = this.state.players[0].nickname;
				const currentPlayer = getPlayer(historyItem.activePlayerId)?.nickname || 'Personne';
				const cardName = (typeof historyItem.card === 'number') && getCardName(historyItem?.card);
				const targetPlayer = historyItem.targetPlayerId && getPlayer(historyItem.targetPlayerId)?.nickname;
				switch(historyItem.name) {
					case "WAITING_PLAYERS": return `En attente de joueur. ${adminNickname} doit ensuite lancer la partie`;
					case "TAKING_A_CARD": return `${currentPlayer} doit prendre une carte.`;
					case "CHOOSING_A_CARD": return `${currentPlayer} doit choisir une carte.`;
					case "APPLIED_EFFECTLESS_CARD": return `${currentPlayer} a joué la carte : ${cardName}`;
					case "APPLYING_CARD_EFFECT":
						return `${currentPlayer} doit appliquer l'effet de sa carte ${cardName}`;
					case "APPLIED_GUARD":
						return `${currentPlayer} a joué la carte : Garde : il a essayé de deviner la carte ${cardName} de ${targetPlayer}`;
					case "APPLIED_PRIEST":
						return `${currentPlayer} a joué la carte : Prêtre : il a vu la carte de ${targetPlayer}`;
					case "APPLIED_BARON":
						return `${currentPlayer} a joué la carte : Baron : il a la même carte que ${targetPlayer}`;
					case "APPLIED_PRINCE":
						return `${currentPlayer} a joué la carte : Prince : il a fait défausser la carte ${cardName} de ${targetPlayer}`;
					case "APPLIED_CHANCELOR":
						return `${currentPlayer} a joué la carte : Chancelier`;
					case "APPLIED_REY":
						return `${currentPlayer} a joué la carte : Roi : il a échangé sa carte avec celle de ${targetPlayer}`;
					
					case "KILL_PLAYER":
						switch(historyItem.reason) {
							case "DEAD_BY_GUARD":
								return `${currentPlayer} a joué la carte : Garde : il a tué ${targetPlayer} en devinant sa carte ${cardName}`;
							case "DEAD_BY_HIS_BARON": 
								return `${currentPlayer} a joué la carte : Baron : il a tué ${targetPlayer} qui avait la carte ${cardName}`;
							case "DEAD_BY_MY_BARON": 
								return `${currentPlayer} a joué la carte : Baron : il est mort contre ${targetPlayer}. Il avait la carte ${cardName}`;
							case "DEAD_BY_DISCARD_PRINCESS": 
								return `${currentPlayer} a joué la carte : Prince : il a tué ${targetPlayer} qui a défaussé sa carte ${cardName}`;
						}
						return `${currentPlayer} à tué le joueur ${targetPlayer}`;
						
					case "END_GAME":
						let winners = historyItem.winnerIds.map((winnerId) => getPlayer(winnerId));
						if(winners) {
							winners = winners.map((player) => player.nickname).join(', ');
						} else {
							winners = "Personne";
						}
						const spy = historyItem.winnerSpyId && getPlayer(historyItem.winnerSpyId);
						
						let spyInfo = '';
						if (!!spy) {
							spyInfo = `L'espion ${spy.nickname} gagne un jeton en plus.`;
						}
						return `${winners} à gagné un jeton. ${spyInfo}`;
						
					default: return historyItem.name;
				}	
			}
		};
		
		return (<div className={getClass()} key={index}>{getMessage()}</div>);
	}

	scrollHistoryToBottom() {
   		this.el.scrollIntoView({ behavior: 'smooth' });
	}
	
	render() {
		const isLastCardSix = this.state.player?.lastUsedCard === 6;
		const hasSelectedAllCards = (this.state.player?.currentCards.length - this.state.actionSixCardsIndex.length) === 1;
		const showActionSixInfo = this.state.isApplyingEffect && isLastCardSix;
		const showActionSixApplyEffectButton = showActionSixInfo && hasSelectedAllCards;

		let getActionSixSelectedCardIndex = (cardIndex) => {
			const selectedIndex = this.state.actionSixCardsIndex.indexOf(cardIndex);
			if (showActionSixInfo && selectedIndex > -1) {
				return <span className="Infos__player__current-cards__card__selected-index">{selectedIndex + 1}</span>
			}
		}

		let getPlayerCardClassList = (index) => {
			let classList = 'Infos__player__current-cards__card';
			if (showActionSixInfo) {
				classList += ' Infos__player__current-cards__card--is-selectable';

				if (this.state.actionSixCardsIndex.includes(index)) {
					classList += ' Infos__player__current-cards__card--is-selected';
				}
			}
			return classList;
		};

		return (
			<div className="Infos">

				{/* Bouton lancement partie */}
				{this.state.showStartGameButton ?
					<button className="Infos__start-button" onClick={this.state.onStartButtonClicked}>Lancer la partie</button> :
					null
				}

				{/* Action du joueur */}
				<div className="Infos__player">
					{/* Infos carte 6 */}
					{showActionSixInfo ?
						<p className="Infos__player__action-six-info">
							La carte 1 sera à la fin du paquet et la carte 2 sera au dessus la carte 1.
						</p> : null
					}

					{/* Carte du joueur */}
					<div className="Infos__player__current-cards">
						{(this.state.player?.currentCards || []).map((currentCard, index) =>
							<div
								style={{ backgroundImage: `url(/love-letter/${currentCard}.png)` }}
								key={`Infos__player__current-cards__card-${index}`}
								className={getPlayerCardClassList(index)}
								onClick={() => showActionSixInfo ?
									this.setActionSixCardsIndex(index) :
									this.state.onCurrentCardClicked(index)}>
								{getActionSixSelectedCardIndex(index)}
								{currentCard}
							</div>
						)}
					</div>

					{/* Bouton de validation action 6 */}
					{showActionSixApplyEffectButton ?
						<button
							className="Infos__player__action-six-button"
							onClick={() => this.onActionSixButtonClicked(this.state.actionSixCardsIndex)}>
							Sélectionner
						</button> : null
					}
				</div>
				<hr />

				{/* Historique de jeu */}
				<div className="Infos__history">
					{this.state.history.slice(0, this.state.history.length - 1).map((historyItem, index) =>this.getActionMessage(historyItem, index))}
					<div ref={el => { this.el = el; }} />
				</div>

				{/* Action en cours */}
				<div className="Infos__action">
					{this.state.roomState ?
						this.getActionMessage(this.state.roomState) :
						'En attente du lancement de la partie...'
					}
				</div>
			</div>
		);
	};
}

export default Infos;
