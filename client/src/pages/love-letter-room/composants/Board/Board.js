import React from 'react';

import "./Board.css";
import Player from '../Player/Player';

class Board extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			id: props.id,
			players: props.players,
			player: props.player,
			roomState: props.roomState,
			deck: props.deck,
			isApplyingEffect: props.isApplyingEffect,
			onDeckCardClicked: props.onDeckCardClicked,
			onPlayerSelection: props.onPlayerSelection
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			id: nextProps.id,
			players: nextProps.players,
			player: nextProps.player,
			roomState: nextProps.roomState,
			deck: nextProps.deck,
			onDeckCardClicked: nextProps.onDeckCardClicked,
			isApplyingEffect: nextProps.isApplyingEffect
		});
	}

	render() {
		return (
			<div className="Board" id={this.state.id}>
				{this.state.players.map((player, index) => {
					return <Player
						key={`${player.id}-${index}`}
						player={player}
						isApplyingEffect={this.state.isApplyingEffect}
						isPlaying={player.id === this.state.roomState?.activePlayerId}
						onSelection={this.state.onPlayerSelection}
					></Player>;
				})}
				<div className="Board-deck-container">
					{!!this.state.deck ? <div className="Board-deck" onClick={this.state.onDeckCardClicked}>{this.state.deck}</div> : null}
				</div>
			</div>
		);
	};
}

export default Board;
