const Client = require("./Client.js");
module.exports = class Player extends Client {
	
	constructor(client, roomId) {
		super(client.io, client.socket, client.nickname);
		this.roomId = roomId;
		
	}
};