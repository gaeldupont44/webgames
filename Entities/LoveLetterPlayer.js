const Player = require("./Player.js");
module.exports = class LoveLetterPlayer extends Player {
	
	constructor(client, roomId) {
		super(client, roomId);
		this.usedCards = [];
		this.currentCards = [];
		this.isAlive = false;
		this.isPlaying = false;
		this.win = 0;
		this.isLastWithoutEffect = false;
	}
	
	initPlay() {
		this.usedCards = [];
		this.currentCards = [];
		this.emitChange();
	}
	
	getInfo() {
		return {
			id: this.id,
			nickname: this.nickname,
			isAlive: this.isAlive,
			isPlaying: this.isPlaying,
			win: this.win,
			usedCards: this.usedCards,
			nbCurrentCards: this.currentCards.length,
			lastUsedCard: this.usedCards[this.usedCards.length - 1],
			isLastWithoutEffect: this.isLastWithoutEffect
		};
	}
	
	addCurrentCard(card) {
		if(typeof card === "number") {
			this.currentCards.push(card);
			this.emitChange();
		}
	}
	
	getCurrentCard() {
		return this.currentCards[0];
	}
	
	getCurrentCards() {
		return this.currentCards;
	}
	
	getLastUsedCard() {
		return this.usedCards[this.usedCards.length - 1];
	}
	
	useCurrentCard(currentCardIndex, isLastWithoutEffect) {
		const cardToUse = this.currentCards[currentCardIndex];
		this.usedCards.push(cardToUse);
		this.currentCards.splice(currentCardIndex, 1);
		this.isLastWithoutEffect = !!isLastWithoutEffect;
		this.emitChange();
	}
	
	onTakeCard(callback) {
		this.socket.on(`room/${this.roomId}/player/takeCard`, () => {
			callback();
		});
	}
	
	onUseCurrentCard(callback) {
		this.socket.on(`room/${this.roomId}/player/useCurrentCard`, (currentCardIndex) => {
			callback(currentCardIndex);
		});
	}
	
	onApplyCardEffect(callback) {
		this.socket.on(`room/${this.roomId}/player/applyCardEffect`, (data) => {
			callback(data);
		});
	}
	
	emitChange() {
		this.socket.emit(`room/${this.roomId}/player`, {
			id: this.id,
			nickname: this.nickname,
			isAlive: this.isAlive,
			isPlaying: this.isPlaying,
			win: this.win,
			usedCards: this.usedCards,
			currentCards: this.currentCards,
			lastUsedCard: this.usedCards[this.usedCards.length - 1],
			isLastWithoutEffect: this.isLastWithoutEffect
		});
	}
	
	
	// PRINCESS
	kill() {
		this.isAlive = false;
		this.emitChange();
	}
	
	resetUsedCards() {
		this.usedCards = [];
		this.emitChange();
	}
	
	// REY
	replaceCurrentCard(card) {
		this.currentCards = [card];
		this.emitChange();
	}
	
	// CHANCELOR
	takeCardsByIndex(cardsIndex) {
		const cards = cardsIndex.map((cardIndex) => this.currentCards[cardIndex]);
		this.currentCards = this.currentCards.filter((card, cardIndex) => !cardsIndex.includes(cardIndex));
		this.emitChange();
		console.log('chancelor', cardsIndex, cards, this.currentCards);
		return cards;
	}
	
	showPlayerCurrentCard(targetPlayerId, targetPlayerCurrentCard) {
		this.socket.emit(`room/${this.roomId}/player/showPlayerCurrentCard`, {targetPlayerId, targetPlayerCurrentCard});
	}
	
	addWin() {
		this.win++;
		this.emitChange();
	}
};