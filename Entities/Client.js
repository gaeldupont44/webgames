module.exports = class Client {
	
	constructor(io, socket, nickname) {
		this.io = io;
		this.socket = socket;
		this.nickname = nickname;
		this.id = socket.id;
		this._listen(socket);
	}
	
	getNickname() {
		return this.nickname;
	}
	
	setNickname(nickname) {
		this.nickname = nickname;
		console.log('set nickname', nickname);
		this.socket.emit("client/nickname/success", nickname);
	}
	
	_listen(socket) {
		this.socket.on("client/nickname", this.setNickname.bind(this));
	}
};