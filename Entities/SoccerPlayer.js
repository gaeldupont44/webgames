const Player = require("./Player.js");
module.exports = class SoccerPlayer extends Player {
	
	constructor(client, roomId) {
		super(client, roomId);
		this.team=null;
		this.position = null;
	}
	
	getInfo(isAdmin) {
		return {
			isAdmin: isAdmin,
			id: this.id,
			nickname: this.nickname,
			team: this.team,
			position: this.position
		};
	}
	
	getTeam() {
		return this.team;
	}
	
	setTeam(team) {
		this.team = team;
		this.socket.emit(`room/${this.roomId}/player/setTeam/success`);
	}
	
	getPosition() {
		return this.position;
	}
	
	setPosition(position) {
		this.position = position;
		this.socket.emit(`room/${this.roomId}/player/setPosition/success`, this.position);
	}
	
	subscribeChangeTeam(callback) {
		console.log('subscribeChangeTeam');
		this.socket.on(`room/${this.roomId}/player/setTeam`, (team) => {
			this.setTeam(team);
			callback(team);
			console.log('setTeam', team);
		});
	}
	
	subscribeChangePosition(callback) {
		this.socket.on(`room/${this.roomId}/player/setPosition`, (position) => {
			this.setPosition(position);
			callback(this.position);
			console.log('setPosition', this.position);
		});
	}
};