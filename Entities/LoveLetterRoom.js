const LoveLetterPlayer = require("./LoveLetterPlayer.js");
const LoveLetterDeck = require("./LoveLetterDeck.js");
const Room = require("./Room.js");



module.exports = class LoveLetterRoom extends Room {
	
	constructor(io, name, password) {
		super(io, name, password, "LOVE_LETTER");
		this.history = [];
	}
	
	addPlayer(client) {
		console.log('addPlayer', client.id);
		let player = new LoveLetterPlayer(client, this.id);
		
		this.players.push(player);
		
		player.socket.on(`room/${this.id}/start`, (data) => {
			if(player.id === this.players[0].id) {
				this.start(data);	
			}
		});
		player.emitChange();
		this.notifyPlayers();
		
	}
	
	removePlayer(clientId) {
		console.log('removePLayer', clientId);
		this.players = this.players.filter((player) => player.id !== clientId);
		if(this.players.length === 0) {
			this.setState({name: "WAITING_PLAYERS"});
		}
		this.notifyPlayers();
	}
	
	getPlayers() {
		return this.players;
	}
	
	getPlayer(playerId) {
		return this.players.find((player) => player.id === playerId);
	}
	
	notifyPlayers() {
		this.io.emit(`room/${this.id}/players`,  this.players.map((player, index) => {
			return {
				...player.getInfo(),
				isAdmin: index === 0
			};
		}));
	}
	
	start(data) {
		this.history = [];
		
		this.setState({name: "STARTING"});
		
		this.initDeck();
		
		this.players.forEach((player) => {
			player.isPlaying = true;
			player.isAlive = true;
			player.replaceCurrentCard(this.deck.takeCard());
			player.resetUsedCards();
		});
		this.players.forEach((player) => this.listenPlayer(player));
		
		const firstPlayerIndex = Math.floor(Math.random() * Math.floor(this.players.length));
		this.setState({
			name: "TAKING_A_CARD",
			activePlayerId: this.players[firstPlayerIndex].id
		});
		
		this.notifyPlayers();
		
	}
	
	initDeck() {
		this.deck = new LoveLetterDeck();
		this.deck.subscribeNbCards((nbCards) => {
			this.io.emit(`room/${this.id}/deck`, nbCards);
		});
		this.deck.burnCard();
	}
	
	setState(state) {
		this.state = state;
		this.updateHistory(state);
		this.io.emit(`room/${this.id}/state`, state);
	}
	
	updateHistory(event) {
		this.history.push(event);
		this.io.emit(`room/${this.id}/history`, this.history);
	}
	
	getNextPlayerIndex(currentPlayerId) {
		const currentPlayerIndex = this.players.findIndex((player) => currentPlayerId === player.id);
		let nextPlayerIndex;
		
		if(currentPlayerIndex < (this.players.length - 1)) {
			nextPlayerIndex = (currentPlayerIndex + 1);
		} else {
			nextPlayerIndex = 0;
		}
		
		if(this.players[nextPlayerIndex].isAlive) {
			return nextPlayerIndex;
		} else {
			return this.getNextPlayerIndex(this.players[nextPlayerIndex].id);
		}
	}
	
	nextPlayer() {
		const alivePlayers = this.players.filter((player) => player.isAlive);
		if(alivePlayers.length === 0) {
			this.setState({
				name: "END_GAME",
				winnerIds: []
			});
		} else if(alivePlayers.length === 1) {
			alivePlayers[0].addWin();
			
			const spyWinner = this.spyHandler();
			
			this.setState({
				name: "END_GAME",
				winnerIds: [alivePlayers[0].id],
				winnerSpyId: spyWinner && spyWinner.id
			});
		} else if(this.deck.getNbCards() === 0) {
			let bestCard;
			alivePlayers.forEach((alivePlayer) => {
				const currentCard = alivePlayer.getCurrentCard();
				if(typeof bestCard !== 'number' || currentCard > bestCard) {
					bestCard = currentCard;
				}
			});
			const winnerPlayers = alivePlayers.filter((alivePlayer) => {
				const currentCard = alivePlayer.getCurrentCard();
				return bestCard === currentCard;
			});
			
			winnerPlayers.forEach((winnerPlayer) => {
				winnerPlayer.addWin();
			});
			
			const spyWinner = this.spyHandler();
			
			this.setState({
				name: "END_GAME",
				winnerIds: winnerPlayers.map((player) => player.id),
				winnerSpyId: spyWinner && spyWinner.id
			});
			
		} else {
			const nextPlayerIndex = this.getNextPlayerIndex(this.state.activePlayerId);
			this.setState({
				name: "TAKING_A_CARD",
				activePlayerId: this.players[nextPlayerIndex].id
			});
		}
		this.notifyPlayers();
	}
	
	spyHandler() {
		const alivePlayers = this.players.filter((player) => player.isAlive);
		const spyAlivePlayers = alivePlayers.filter((alivePlayer) => alivePlayer.usedCards.some((usedCard) => usedCard === 0));
		if(spyAlivePlayers.length === 1) {
			spyAlivePlayers[0].addWin();
			return spyAlivePlayers[0];
		}
	}
	
	listenPlayer(player) {
		player.onTakeCard(() => {
			if(this.state.activePlayerId === player.id && this.state.name === "TAKING_A_CARD") {
				player.addCurrentCard(this.deck.takeCard());
				this.setState({
					name: "CHOOSING_A_CARD",
					activePlayerId: player.id
				});
				this.notifyPlayers();
			}
		});
		
		player.onUseCurrentCard((currentCardIndex) => {
			
			const currentCardToUse = player.getCurrentCards()[currentCardIndex];
			const isNotAllowUseCard = player.getCurrentCards().includes(8) && ((currentCardToUse === 5) || (currentCardToUse === 7));
			
			if(this.state.activePlayerId === player.id && this.state.name === "CHOOSING_A_CARD" && !isNotAllowUseCard) {
				player.useCurrentCard(currentCardIndex);
				const lastUsedCard = player.getLastUsedCard();
				
				if(lastUsedCard === 0 || lastUsedCard === 4 || lastUsedCard === 8) {
					this.updateHistory({
						name: "APPLIED_EFFECTLESS_CARD",
						activePlayerId: player.id,
						card: lastUsedCard
					});
					this.nextPlayer();
				}
				
				if(lastUsedCard === 6) {
					player.addCurrentCard(this.deck.takeCard());
					player.addCurrentCard(this.deck.takeCard());
					if(player.getCurrentCards().length > 1) {
						this.setState({
							activePlayerId: player.id,
							card: lastUsedCard,
							name: "APPLYING_CARD_EFFECT"
						});
					} else {
						this.nextPlayer();
					}
					
				}
				
				if(lastUsedCard === 1 || lastUsedCard === 2 || lastUsedCard === 3 || lastUsedCard === 5 || lastUsedCard === 7) {
					const playerSelectable = this.players.filter((_player) => {
						if(_player.isAlive && (_player.getLastUsedCard() !== 4 || !!_player.isLastWithoutEffect)) {
							const isCurrentPlayer = _player.id === player.id;
							return lastUsedCard === 5 ? true : !isCurrentPlayer;
						}
					});
					if(playerSelectable.length > 0) {
						this.setState({
							activePlayerId: this.state.activePlayerId,
							card: lastUsedCard,
							name: "APPLYING_CARD_EFFECT"
						});
					} else {
						this.nextPlayer();
					}
				}
				
				if(lastUsedCard === 9) {
					player.kill();
					this.nextPlayer();
				}	
			}
			
			this.notifyPlayers();
		});
		
		
		player.onApplyCardEffect((data) => {
			
			const sevenAction = (data) => {
				const targetPlayer = this.getPlayer(data.playerId);
				const targetPlayerCard = targetPlayer.getCurrentCard();
				const playerCard = player.getCurrentCard();
				targetPlayer.replaceCurrentCard(playerCard);
				player.replaceCurrentCard(targetPlayerCard);
				this.updateHistory({
					name: "APPLIED_REY",
					activePlayerId: this.state.activePlayerId,
					targetPlayerId: targetPlayer.id
				});
			};
			
			const sixAction = (cardsIndex) => {
				const cards = player.takeCardsByIndex(cardsIndex);
				this.deck.putCardsBelow(cards);
				this.updateHistory({
					name: "APPLIED_CHANCELOR",
					activePlayerId: this.state.activePlayerId
				});
			};
			
			const fiveAction = (data) => {
				const targetPlayer = this.getPlayer(data.playerId);
				targetPlayer.useCurrentCard(0, true);
				const targetLastUsedCard = targetPlayer.getLastUsedCard();
				if(targetLastUsedCard === 9) {
					targetPlayer.kill();
					this.updateHistory({
						name: "KILL_PLAYER",
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id,
						card: targetLastUsedCard,
						reason: "DEAD_BY_DISCARD_PRINCESS"
					});
				} else {
					targetPlayer.addCurrentCard(this.deck.takeCard());
					this.updateHistory({
						name: "APPLIED_PRINCE",
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id,
						card: targetLastUsedCard
					});
				}
			};
			
			const threeAction = (data) => {
				const targetPlayer = this.getPlayer(data.playerId);
				
				const targetCurrentCard = targetPlayer.getCurrentCard();
				const currentCard = player.getCurrentCard();
				
				targetPlayer.showPlayerCurrentCard(player.id, currentCard);
				player.showPlayerCurrentCard(targetPlayer.id, targetCurrentCard);
				
				if(targetCurrentCard < currentCard) {
					targetPlayer.useCurrentCard(0, true);
					targetPlayer.kill();
					this.updateHistory({
						name: "KILL_PLAYER",
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id,
						card: targetCurrentCard,
						reason: "DEAD_BY_HIS_BARON"
					});
				} else if (currentCard < targetCurrentCard){
					player.useCurrentCard(0, true);
					player.kill();
					this.updateHistory({
						name: "KILL_PLAYER",
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id,
						card: currentCard,
						reason: "DEAD_BY_MY_BARON"
					});
				} else {
					this.updateHistory({
						name: "APPLIED_BARON",
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id
					});
				}
			};
			
			const twoAction = (data) => {
				const targetPlayer = this.getPlayer(data.playerId);
				const targetCurrentCard = targetPlayer.getCurrentCard();
				player.showPlayerCurrentCard(targetPlayer.id, targetCurrentCard);
				this.updateHistory({
					name: "APPLIED_PRIEST",
					activePlayerId: this.state.activePlayerId,
					targetPlayerId: targetPlayer.id
				});
			};
			
			const oneAction = (targetInfo) => {
				const targetPlayer = this.getPlayer(targetInfo.playerId);
				const isTargetCorrect = targetPlayer.getCurrentCard() === targetInfo.card;
				
				if(isTargetCorrect) {
					targetPlayer.useCurrentCard(0, true);
					targetPlayer.kill();
					this.updateHistory({
						name: "KILL_PLAYER",
						card: targetInfo.card,
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id,
						reason: "DEAD_BY_GUARD"
					});
				} else {
					this.updateHistory({
						name: "APPLIED_GUARD",
						card: targetInfo.card,
						activePlayerId: this.state.activePlayerId,
						targetPlayerId: targetPlayer.id
					});
				}
			};
		
			if(this.state.activePlayerId === player.id && this.state.name === "APPLYING_CARD_EFFECT") {
				switch (this.state.card) {
					case 1:
						oneAction(data);
						break;
					case 2:
						twoAction(data);
						break;
					case 3:
						threeAction(data);
						break;
					case 5:
						fiveAction(data);
						break;
					case 6:
						sixAction(data);
						break;
					case 7:
						sevenAction(data);
						break;
				}
				this.nextPlayer();
				this.notifyPlayers();
			}
		});
		
	}
	
};