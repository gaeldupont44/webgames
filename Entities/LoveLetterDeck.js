module.exports = class LoveLetterDeck {
	
	constructor() {
		this.initCards =  [0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 9];
		this.cards = this.shuffle(this.initCards);
		this.burnedCards = [];
		this.onNbCardsChange = () => {};
	}
	
	shuffle(initCards) {
		const cards = [...initCards];
		for (let i = cards.length - 1; i > 0; i--) {
        	const j = Math.floor(Math.random() * (i + 1));
        	[cards[i], cards[j]] = [cards[j], cards[i]];
    	}
    	return cards;
	}
	
	burnCard() {
		if(this.cards.length > 0) {
			this.burnedCards.push(this.cards.pop());
			this.onNbCardsChange(this.getNbCards());
		}
	}
	
	getNbCards() {
		return this.cards.length;
	}
	
	takeCard() {
		if(this.cards.length > 0) {
			const card = this.cards.pop();
			this.onNbCardsChange(this.getNbCards());
			return card;
		}
	}
	
	putCardsBelow(cards) {
		this.cards.unshift(...cards);
		this.onNbCardsChange(this.getNbCards());
	}
	
	subscribeNbCards(callback) {
		this.onNbCardsChange = callback;
	}
	
};