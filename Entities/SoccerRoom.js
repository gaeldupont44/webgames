const SoccerPlayer = require("./SoccerPlayer.js");
const Room = require("./Room.js");
module.exports = class SoccerRoom extends Room {
	
	constructor(io, name, password) {
		super(io, name, password, "SOCCER");
	}
	
	setState(state) {
		console.log('setstate', state);
		if(this.state !== state) {
			this.state = state;
			console.log('emitting new state', state);
			this.io.emit(`room/${this.id}/state`, state);
		}
	}
	
	addPlayer(client) {
		let player = new SoccerPlayer(client, this.id);
		
		this.players.push(player);
		
		player.subscribeChangeTeam((team) => {this.notifyPlayers()});
		
		player.socket.on(`room/${this.id}/start`, (data) => this.start(data));
		
		this.notifyPlayers();
	}
	
	removePlayer(clientId) {
		this.players = this.players.filter((player) => player.id !== clientId);
		this.notifyPlayers();
	}
	
	getPlayers() {
		return this.players;
	}
	
	notifyPlayers() {
		console.log('notifying players',  this.players.map((player, index) => player.getInfo(index === 0)));
		this.io.emit(`room/${this.id}/players`,  this.players.map((player, index) => player.getInfo(index === 0)));
	}
	
	start(data) {
		console.log('start', data);
		this.setState("PLAYING");
		this.players.forEach((player) => {
			player.socket.emit(`room/${this.id}/data`, {
				state: this.state,
				duration: data.duration || 120
			});
		});
	}
	
};