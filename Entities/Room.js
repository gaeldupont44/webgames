const SoccerPlayer = require("./SoccerPlayer.js");
const LoveLetterPlayer = require('./LoveLetterPlayer.js');
module.exports = class Room {
	
	constructor(io, name, password, type) {
		this.id = `room-${new Date().getTime()}`;
		this.io = io;
		this.name = name;
		this.password = password;
		this.type = type;
		this.players = [];
		this.state = "WAITING_PLAYERS";
	}
};