const webSocketsServerPort = 8083;

var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http); //, { pingTimeout: 1000, pingInterval: 2000 }x

const Room = require("./Entities/Room.js");

const SoccerRoom = require("./Entities/SoccerRoom.js");
const LoveLetterRoom = require("./Entities/LoveLetterRoom.js");
const Client = require("./Entities/Client.js");

app.get('/', (req, res) => {
  res.json({hello: 'world'});
});

let clients = [];

let rooms = [];


formatRoom = (room) => {
	return {
		id: room.id,
		name: room.name,
		type: room.type,
		password: room.password,
		nbPlayer: room.players.length
	};
};

emitRooms = (socket) => {
	const emitter = socket || io;
	emitter.emit("rooms", rooms.map(formatRoom));
};

addRoom = (room) => {
	if(room) {
		rooms.push(room);
		emitRooms();
	}
};

io.on('connection', (socket) => {
  console.log('a user connected', socket.id);
  
  const client = new Client(io, socket);
  
  clients.push(client);
  
  socket.on("disconnect", () => {
	console.log("Client disconnected");
	rooms.forEach((room) => {
	  	const hasPlayer = room.getPlayers().some((player) => {
	  		return player.id === client.id;
	  	});
	  	if(hasPlayer) {
			room.removePlayer(client.id);
			emitRooms();
	  	}
	  });
  });
  
  
  socket.on("rooms", () => {
	emitRooms(socket);
  });
  
  socket.on("room/create", (roomData) => {
  	console.log('creating room', roomData.name);
  	const existingRoom = rooms.some((room) => room.name === roomData.name);
  	if(existingRoom) {
  		socket.emit("room/create/error", "ROOM_ALREADY_EXIST");
  		return;
  	}
  	let room;
  	switch (roomData.type) {
  		case "SOCCER":
  			room = new SoccerRoom(io, roomData.name, roomData.password);		
  			break;
  		case "LOVE_LETTER":
  			room = new LoveLetterRoom(io, roomData.name, roomData.password);
  			break;
  	}
  	if(!room) {
  		socket.emit("room/create/error", "ROOM_TYPE_UNKNOWN");	
  	}
  	addRoom(room);
  	socket.emit("room/create/success", {id: room.id, type: room.type});
  });
  
  socket.on("room/connect", (roomId) => {
  	console.log('connecting room', roomId);
  	const room = rooms.find((room) => room.id === roomId);
  	if(room) {
  		socket.emit("room/connect/success", room.id);
  		room.addPlayer(client);
  		emitRooms();
  	} else {
  		socket.emit("room/connect/error", "ROOM_NOT_FOOND");
  	}
  	
  });
  
  socket.on("room/disconnect", (roomId) => {
  	const room = rooms.find((room) => room.id === roomId);
  	if(room) {
  		socket.emit("room/connect/success", room.id);
  		room.removePlayer(client.id);
  		emitRooms();
  	} else {
  		socket.emit("room/disconnect/error", "ROOM_NOT_FOOND");
  	}
  });
  
});

http.listen(webSocketsServerPort, () => {
  console.log(`listening on *:${webSocketsServerPort}`);
});
